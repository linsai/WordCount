package jframe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class WordCount {
	
	private int lineCount;   // 代码行数  
	private int wordCount;   //单词个数
	private int charCount;   //字符个数
	private int codeCount;   //代码行
	private int noteCount;   //注释行
	private int spaceCount;  //空行
	static String endStr=".c";//递归文件后缀，默认.c
	static String getPath=".";//递归路径
	static boolean useStop=false;//是否使用stoplist

	//构造函数
	public WordCount(int lineCount,int wordCount,int charCount,int CodeCount,int noteCount,int spaceCount){
		this.lineCount=lineCount;
		this.wordCount=wordCount;
		this.charCount=charCount;
		this.codeCount=CodeCount;
		this.noteCount=noteCount;
		this.spaceCount=spaceCount;
	}
    //get和set方法
	public int getCodeCount() {
		return codeCount;
	}
	public void setCodeCount(int codeCount) {
		this.codeCount = codeCount;
	}
	public int getNoteCount() {
		return noteCount;
	}
	public void setNoteCount(int noteCount) {
		this.noteCount = noteCount;
	}
	public int getSpaceCount() {
		return spaceCount;
	}
	public void setSpaceCount(int spaceCount) {
		this.spaceCount = spaceCount;
	}
	public int getLineCount() {
		return lineCount;
	}
	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}
	public int getWordCount() {
		return wordCount;
	}
	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}
	public int getCharCount() {
		return charCount;
	}
	public void setCharCount(int charCount) {
		this.charCount = charCount;
	}
	
	
//main函数	
	public static void main(String[] args) throws IOException{
		//初始化参数
		WordCount WC=new WordCount(0,0,0,0,0,0);
		//默认文件
		String inputFile="file.c";
		String stopFile=null;
		String outputFile="result.txt";
		
		//获取参数路径
		for(int i=0;i<args.length;i++){	
			//inputFile
			if(args[i].endsWith(".c"))
				inputFile=args[i];
			//stopFile
			if(args[i].equals("-e")){
				useStop=true;
				if((i!=(args.length-1))&&args[i+1].endsWith(".txt")){
					stopFile=args[i+1];
					File file=new File(stopFile);
					if(!file.exists()){
						System.out.println("停用词表"+stopFile+"文件不存在");
						System.exit(0);
					}
				}	
				else{
					System.out.println("请输入.txt文件作为停用词表！");
					System.exit(0);
				}
			}	
			//outputFile
			if(args[i].equals("-o")){
				if(i!=(args.length-1))
					outputFile=args[i+1];
				else{
					System.out.println("请输入输出文件名！");
					System.exit(0);
				}
			}
				
		}
		
		//是否输入-s命令
		boolean flag=false;
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-s")) 
				flag=true;	
		}
		
		//输入-s后获得输入文件后缀和路径
		if(flag){
			for(int i=0;i<args.length;i++){
				if((args[i].indexOf("*")!=-1)){
					endStr=args[i].substring(args[i].lastIndexOf("*")+1, args[i].length());
				    getPath=args[i].substring(0,args[i].lastIndexOf("*"));
				   if(getPath.equals(""))
				    	getPath=".";
				}
			}
		}
		
		//有-s递归处理文件
		if(flag){
			File dir = new File(getPath);// 你的工作站  
			List<File> files = getFile(dir); // .c文件的集合
			    for (File file : files) { 
	        	WordCount WCC=new WordCount(0,0,0,0,0,0);
	        	String filePath = file.getAbsolutePath();
	        	WCC.wc(filePath,stopFile);
	        	WCC.command(args, filePath, outputFile, WCC);
	        }
			   
		}
		//无-s处理InputFile
		else{
			WC.wc(inputFile,stopFile);
			WC.command(args, inputFile, outputFile, WC);
		}
		//写命令执行日期到outputFile
		    File writename = new File(outputFile); 
	    	writename.createNewFile(); 
	        BufferedWriter out = new BufferedWriter(new FileWriter(writename,true));
	        Date day=new Date();    
			SimpleDateFormat df = new SimpleDateFormat("-------------------------执行日期：yyyy-MM-dd HH:mm:ss\r\n"); 
			out.write(df.format(day));  
			// 把缓存区内容压入文件 
			out.flush();  
	        out.close(); 
	}
	
	
//命令执行，根据命令输出数据到屏幕和outputFile中	
	public void command(String[] args,String inputFile,String outputFile,WordCount WC)throws IOException{
		String outResult="";
		inputFile=inputFile.substring(inputFile.lastIndexOf("\\")+1, inputFile.length());
		/*System.out.println(inputFile+"：字符数:"+WC.getCharCount());
		System.out.println(inputFile+"：字行数:"+WC.getLineCount());
		System.out.println(inputFile+"：单词数:"+WC.getWordCount());
		System.out.println(inputFile+"注释标记数："+WC.getNoteCount());*/
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-c"))
				outResult=outResult+inputFile+",字符数:" + WC.getCharCount()+"\r\n";
		}
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-w"))
				outResult=outResult+inputFile+",单词数:" + WC.getWordCount()+"\r\n";
		}
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-l"))
				outResult=outResult+inputFile+",行数:" + WC.getLineCount()+"\r\n";
		}
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-a"))
				outResult=outResult+inputFile+",代码行/空行/注释行:"+WC.getCodeCount()+","
						+WC.getSpaceCount()+","+WC.getNoteCount()+"\r\n";
		}
		//调用图形化界面，UI.exe保存于D:/WordCount/test/UI.exe路径下
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-x"))
			{
				Runtime rn = Runtime.getRuntime();
				Process p = null;
				try {
					p=rn.exec("\"D:/WordCount/test/UI.exe\"");
				
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		//写数据到outputFile
		System.out.println(outResult);		
		File writename = new File(outputFile); 
    	writename.createNewFile(); 
        BufferedWriter out = new BufferedWriter(new FileWriter(writename,true));
	    out.write(outResult);
		out.flush();  
        out.close(); 
	}

	
	
//统计功能字符数、单词数、行数、代码行/空行/注释行	
	public void wc(String inputFile,String stopFile) throws IOException{
		String lineString = null; 
        String[] buffer=null; //文件每行
        String[] buffer1 = null;//stoplist
       
        boolean isNote = false;
        int notNote=0;
        
        //读取停用词表
        if(useStop){
        	File dirr=new File(stopFile);
            BufferedReader bff = new BufferedReader(new FileReader(dirr));
            while((lineString=bff.readLine())!=null){
              buffer1=lineString.split(",| ");
            }
            bff.close();	
        }
        lineString = null;
        
       // 读取输入文件inputFile
        File dir=new File(inputFile);
        BufferedReader bf = new BufferedReader(new FileReader(dir));         
        while((lineString=bf.readLine())!=null){
 
        	 //遇到 , 空格 就结束赋值
        	buffer=lineString.split(",|;| ");
        	for(int i=0;i<buffer.length;i++){
        		
        		//使用停用词表则剔除词表内单词，不用则不踢
        		if(useStop){
        			if(!buffer[i].equals("")&&!inStop(buffer[i], buffer1)){
            			wordCount++;
            		} 	
        		}
        		else{
        			wordCount++;
        		}
        		       		
        	}
        	//if(buffer.length!=1)
        		lineCount++;
        	
        	charCount+=(lineString.length()+1);
        	
        	
        	lineString=lineString.trim();
        	//空行，一个字符的也算空行
        	if (lineString.matches("^[//s&&[^//n]]*$")||lineString.length()==1) {  
        		spaceCount++;      
            } 
        	//注释/*的开始
        	else if (lineString.startsWith("/*") && !lineString.endsWith("*/")||((lineString.startsWith("{/*")
        			||lineString.startsWith("}/*"))&&!lineString.endsWith("*/"))){
        		 noteCount++;
        		 isNote=true;
        	 }
        	//没有遇到*/
        	else if(isNote&&!lineString.endsWith("*/")&&!lineString.startsWith("*/")) {  
        		notNote++;
        		noteCount++;
        	}
        	//遇到*/
        	else if (isNote == true && (lineString.endsWith("*/")||lineString.startsWith("*/"))) {
        		noteCount++;
        		isNote=false;
        	}
        	//注释行
        	else if (lineString.startsWith("//")|| lineString.startsWith("}//")||lineString.startsWith("{//")||
        		    ((lineString.startsWith("{/*") ||lineString.startsWith("}/*")||lineString.startsWith("/*"))
        		    		&& lineString.endsWith("*/"))) { 
        		noteCount++;
        	}
        	else{
        		codeCount++;
        	}        	
            }
            bf.close();
            noteCount-=notNote;
            codeCount+=notNote;
	}


//判断是否在停用词表内
public static boolean inStop(String str,String[] buffer){
	int count=0;
	for(int i=0;i<buffer.length;i++){
		if(str.equals(buffer[i])){
			count++;
		}
	}
	if(count>0)
		return true;
	else
		return false;
}
	


//遍历文件	
	public static List<File> getFile(File dir) {  
        List<File> files = new ArrayList<File>();
        // 此文件下的所有文件和文件夹集合
        File[] subs = dir.listFiles();   
        for (File file : subs) {  
            if (file.isFile() && file.getName().endsWith(endStr)) {
            	// 把获取到的后缀文件添加到集合中，可以是任何后缀文件  
                files.add(file); 
            } else if (file.isDirectory()) 
            	//如果是目录，就进行递归
                files.addAll(getFile(file));   
        }  
        return files;  
    }   
}