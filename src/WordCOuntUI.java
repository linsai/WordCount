package jframe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.TextArea;

import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.SpinnerModel;
import javax.swing.JTabbedPane;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Color;
import java.awt.Container;
import java.awt.SystemColor;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import java.awt.ScrollPane;
import java.awt.List;
import javax.swing.JList;
import javax.swing.JSpinner;

public class WordCountUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameTest frame = new FrameTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 639, 632);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(0, 255, 255));
		contentPane.setBackground(new Color(153, 180, 209));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("\u9009\u62E9\u6587\u4EF6\u8DEF\u5F84");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFileChooser chooser = new JFileChooser(); // 设置选择器
				chooser.setMultiSelectionEnabled(true); // 设为多选
				int returnVal = chooser.showOpenDialog(button); // 是否打开文件选择框
				System.out.println("returnVal=" + returnVal);

				if (returnVal == JFileChooser.APPROVE_OPTION) { // 如果符合文件类型

					String filepath = chooser.getSelectedFile().getAbsolutePath();// 获取绝对路径
					//String filename = chooser.getSelectedFile().getName();
					filepath = filepath.replaceAll("\\\\","/");
					textField.setText(filepath);
					//textField_1.setText(filename + "统计结果为：");
					/*System.out.println(filepath);
					System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName()); // 输出相对路径
*/
				}
			}
		});
		button.setBounds(422, 40, 123, 27);
		contentPane.add(button);
		
		ButtonGroup buttonGroup2 = new ButtonGroup();
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 134, 449, 38);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JCheckBox wordcount = new JCheckBox("\u5355\u8BCD\u6570");
		wordcount.setBounds(10, 5, 73, 27);
		panel.add(wordcount);
		
		JCheckBox charcount = new JCheckBox("\u5B57\u7B26\u6570");
		charcount.setBounds(89, 5, 73, 27);
		panel.add(charcount);
		
		JCheckBox lincount = new JCheckBox("\u884C\u6570");
		lincount.setBounds(168, 5, 59, 27);
		panel.add(lincount);
		
		JCheckBox extend = new JCheckBox("\u4EE3\u7801\u884C/\u7A7A\u884C/\u6CE8\u91CA\u884C");
		extend.setBounds(249, 5, 165, 27);
		panel.add(extend);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(14, 252, 532, 255);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(0, 0, 532, 255);
		panel_1.add(textArea);
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(0, 0, 518, 255);
		panel_1.add(scrollPane);

		JButton button_1 = new JButton("\u5F00\u59CB\u67E5\u8BE2");
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource()==button_1){
					WordCount WC=new WordCount(0,0,0,0,0,0);
					String path=textField.getText();
					String inputFile=path;
					String stopFile="result.txt";
					try {
						WC.wc(inputFile, stopFile);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//获取参数路径
					String result = inputFile+"单词数："+WC.getWordCount();
					String result1 = inputFile+"字符数："+WC.getCharCount();
					String result2 = inputFile+"行数："+WC.getLineCount();
					String result3 = inputFile+"代码行/空行/注释行:"+WC.getCodeCount()+","
							+WC.getSpaceCount()+","+WC.getNoteCount();
					//textArea.append(result+"\n"+result1+"\n"+result2+"\n"+result3);
					if(charcount.isSelected()){
						textArea.append(result1+"\n");
					}
					if(wordcount.isSelected()){
						textArea.append(result+"\n");
					}
					if(lincount.isSelected()){
						textArea.append(result2+"\n");
					}
					if(extend.isSelected()){
						textArea.append(result3+"\n");
					}
				}
			}
		});
		
		
		button_1.setBounds(494, 134, 113, 27);
		//buttonGroup2.add(button_1);
		contentPane.add(button_1);

		JLabel label = new JLabel("\u7ED3\u679C\u8F93\u51FA\uFF1A");
		label.setBounds(14, 200, 87, 18);
		contentPane.add(label);

		JLabel lblWc = new JLabel("WC\u5728\u7EBF");
		lblWc.setBounds(14, 10, 72, 18);
		contentPane.add(lblWc);
		
		JLabel label_1 = new JLabel("\u5206\u7C7B\u67E5\u8BE2\uFF1A");
		label_1.setBounds(14, 103, 87, 18);
		contentPane.add(label_1);

		textField = new JTextField();
		textField.setBounds(9, 31, 399, 38);
		contentPane.add(textField);
		textField.setColumns(10);
	}
}

